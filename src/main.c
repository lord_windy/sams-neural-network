#include <stdio.h>
#include <time.h>

#include "network.h"

int main(int argc, char** argv) {
	srand(time(NULL));
	
	Network* n = malloc(sizeof(Network));
	float inputs[4][2] = {{0,0},{0,1},{1,0},{1,1}};
	float answers[] = {1,0,0,1}; //1 = false, 0 = true
	
	init_network(n, 3);
	add_layer(n, 0, 2, 2, SIGMOID);
	add_layer(n, 1, 2, 2, SIGMOID);
	add_layer(n, 2, 2, 2, SIGMOID);

	debug_csv_init(n, 2);

  predict_from_input(n, inputs[0]);
  debug_csv_out(n, inputs[0], 2, answers[0]);
  
  init_dfa_batch_network(n, 2);

  for (int j = 0; j < 500; j++) {
    for (int i = 0; i < 2; i++) {
      exec_dfa_network(n, inputs[0], i);
    }
    learn_dfa_network(n, 0.2);
		predict_from_input(n, inputs[0]);
    debug_csv_out(n, inputs[0], 2, answers[0]);
  }

  printf("Hello World\n");
	return 0;
}

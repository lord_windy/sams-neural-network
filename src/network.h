//
// Created by Samuel Brown on 4/1/20 (Australian Format)
// Rewrite number 5

#pragma once

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

enum NeuronActivations{
  SIGMOID,
  RELU,
};

typedef struct {
	enum NeuronActivations activate_method;
  int input_length;
  float* weights;
  float bias;
  float near_output;
  float output;
  
  //our batch of learning weights
  int batch_size;
  float** learning_batch; //2d array of weights+bias * batch_size
  
  //the matrix for the DFA thingy
  int columns; //doubles as error quantity
  int rows;
  float** eb_matrix; //this is the matrix of e x b
  float* error_output; 

} Neuron;

//Basic Neuron stuff
void init_neuron(Neuron* n, int inputs, enum NeuronActivations activate);
void free_neuron(Neuron* n);
void forward_propagate_neuron(Neuron* n, float* inputs);

//DFA learning
void init_dfa_batch_neuron(Neuron* n, int batch_quantity, int error_quantity);
void exec_dfa_neuron(Neuron* n, float* errors, float* inputs,int batch);
void learn_dfa_neuron(Neuron* n, float learning);
void free_dfa_neuron(Neuron* n); //frees the necessary memory


void test_neurons();

typedef struct {
	FILE* debug_csv;
	int network_depth;
	int* layer_depth;
	Neuron** neurons;
} Network;

//Basic Neural network stuff
void init_network(Network* n, int width);
void add_layer(Network* n, int pos, int length, int inputs,
							enum NeuronActivations type);
int predict_from_input(Network* n, float* inputs);

//DFA learning for a network
void init_dfa_batch_network(Network* n, int batch_quantity);
void exec_dfa_network(Network* n, float* inputs, int batch);
void learn_dfa_network(Network* n, float learning);
void free_dfa_neuron(Neuron* n);

//debug for the output. Will not output the DFA stuff for size reason
void debug_csv_init(Network* n, int input_length);
void debug_csv_out(Network* n, float* inputs, int input_length, int expected);


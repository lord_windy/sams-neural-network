//
// Created by sam on 9/12/19.
//

#include "data.h"

void populate_image(Image* img, FILE* images, FILE* labels){
  img->label = fgetc(labels);
  for (int i = 0; i < IMAGESIZE; i++) {
    float t = (float)fgetc(images);
    if (isnan(t/255.0)) {
      printf("Something went wrong in populating the image");
    }
    img->pixels[i] = t/255.0;
  }

}

void generate_training_set(Dataset* set) {
  FILE* images = fopen("./data/train-images.idx3-ubyte", "r");
  FILE* labels = fopen("./data/train-labels.idx1-ubyte", "r");

  //skip the first bits of useless data
  unsigned char images_buffer[30];
  unsigned char labels_buffer[30];
  fgets(images_buffer, 17, images);
  fgets(labels_buffer, 9, labels);

  //now we can populate everything!

  for (int i = 0; i < TRAINING_NUMBER; i++){
    populate_image(&set->training[i], images, labels);
  }

  //printf("%i %i %i %i %i \n", set->training[0].label, set->training[1].label, set->training[2].label, set->training[3].label, set->training[4].label);
  int s = fgetc(images);
  
  fclose(images);
  fclose(labels);
}

void generate_testing_set(Dataset* set) {
  FILE* images = fopen("./data/t10k-images.idx3-ubyte", "r");
  FILE* labels = fopen("./data/t10k-labels.idx1-ubyte", "r");

  //skip the first bits of useless data
  unsigned char images_buffer[30];
  unsigned char labels_buffer[30];
  fgets(images_buffer, 17, images);
  fgets(labels_buffer, 9, labels);

  for (int i = 0; i < TEST_NUMBER; i++){
    populate_image(&set->testing[i], images, labels);
  }

  //printf("%i %i %i %i %i \n", set->testing[0].label, set->testing[1].label, set->testing[2].label, set->testing[3].label, set->testing[4].label);
  int s = fgetc(images);

  fclose(images);
  fclose(labels);
}

void initiate_data(Dataset** data) {
    *data = malloc(sizeof(Dataset));
    generate_training_set(*data);
    generate_testing_set(*data);
}

//
// Created by sam on 9/12/19.
//

#ifndef NEURALNETWORK_DATA_H
#define NEURALNETWORK_DATA_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define IMAGESIZE 784
#define TEST_NUMBER 10000
#define TRAINING_NUMBER 60000

typedef struct  {
    float pixels[IMAGESIZE];
    unsigned char label;
} Image;

typedef struct {
  Image training[TRAINING_NUMBER];
  Image testing[TEST_NUMBER];
} Dataset;

void initiate_data(Dataset**);


#endif //NEURALNETWORK_DATA_H

#include "network.h"

//Important, srand() must be set
//returns a variable regular standard deviations of -3.5 and 3.5.
//It should be distributed with -1 to 1 being the most popular. 
float std_deviation_random() {

  float random = 0;

  do {
    float u1 = (rand() % 1000) / 1000.0;
    float u2 = (rand() % 1000) / 1000.0;
    float r = sqrt(-2 * log(u1));
    float theta = 2 * M_PI * u2;

    random =r * cos(theta);
  } while (isinf(random) || isnan(random));

  if (isinf(random) || isinf(random))
    printf("Somehow there was a mistake");

  return random;
  
}

//Takes in a prebuilt Neuron and the number of inputs
//It then creates all the weights and biases and sets them to
//the standard deviation random variables
void init_neuron(Neuron* n, int inputs, enum NeuronActivations activate) {
  n->activate_method = activate;
  n->input_length = inputs;
  n->weights = malloc(sizeof(float)*inputs);

  for (int i = 0; i < inputs; i++) {
    n->weights[i] = std_deviation_random();
  }

  n->bias = std_deviation_random();

}

//frees the neuron
void free_neuron(Neuron* n) {
  free(n->weights);
}

void sigmoid_activate(Neuron* n) {
  n->output = 1 / (1 + powf(M_E, -1 * n->near_output));  
}

void RELU_activate(Neuron* n) {
 n->output = n->near_output * ( n->near_output > 0.0 );  
}

void forward_propagate_neuron(Neuron* n, float* inputs) {
  n->near_output = 0;
  
  for (int i = 0; i < n->input_length; i++) {
    n->near_output += n->weights[i] * inputs[i];
  }

  n->near_output += n->bias;

  //activate
  
  switch (n->activate_method) {
    case SIGMOID:
      sigmoid_activate(n);
      break;
    case RELU:
      RELU_activate(n);
      break;
  }

}

//Just makes sure that the inputs are sane
int check_neuron_init(Neuron* n) {
  
  if (n == NULL) {
    printf("Somehow the neuron came in null.\n");
    return 1;
  }

  if (isinf(n->bias) || isnan(n->bias)) {
    printf("Bias came in with bad randoms.\n");
    return 1;
  }

  for (int i = 0; i < n->input_length; i++) {
    if (isinf(n->weights[i]) || isnan(n->weights[i])) {
      printf("Weight came in with bad randoms.\n");
      return 1;
    }
  }
  return 0;
}

void test_neurons() {
  //How do I test this? LOL...
  //Initialize a Neuron test
  Neuron* n = malloc(sizeof(Neuron));
  init_neuron(n, 8, SIGMOID);
  if (check_neuron_init(n)) {
    printf("Exiting the program. Bad init of the Neuron\n");
    exit(1);
  }
  //Check the outputs. 
  
}

void init_network(Network* n, int width) {
  n->debug_csv = fopen("output.csv", "w");
  n->network_depth = width;
  n->neurons = malloc(sizeof(Neuron*) * width);
  n->layer_depth = malloc(sizeof(int) * width);
  for (int i = 0; i < width; i++) {
    n->neurons[i] = NULL;
    n->layer_depth[i] = 0;
  }
}

void add_layer(Network* n, int pos, int length, int inputs, 
              enum NeuronActivations type) {
  if (n->neurons[pos] != NULL) {
    if (n->layer_depth[pos] != 0) {
      printf("Position already used, freeing and reiniting. Should have no memory leak\n");
      
      for (int i = 0; i < n->layer_depth[pos]; i++) {
        free_neuron(&n->neurons[pos][i]);
      }

      n->layer_depth[pos] = 0;
      free(n->neurons[pos]);
    } else {
      printf("Already used position without known length. Reinting, but likely memory leak\n");
      free (n->neurons[pos]);
    }
  }
  
  n->neurons[pos] = malloc(sizeof(Neuron)*length);
  n->layer_depth[pos] = length;
  for (int i = 0; i < length; i++) {
    init_neuron(&n->neurons[pos][i], inputs, type); 
  }

}

int find_max_outputs(Network* n) {
  int max = 0;
  
  for (int i = 0; i < n->network_depth; i++) {
    if (max > n->layer_depth[i]) {
      max = n->layer_depth[i];
    }
  }

  return max;
}

void layer_activate(Network* n, int pos, float* inputs) {
  for (int i = 0; i < n->layer_depth[pos]; i++) {
    forward_propagate_neuron(&n->neurons[pos][i], inputs);
  }
}

void retrieve_outputs(Network* n, int pos, float* buffer) {
  for (int i = 0; i < n->layer_depth[pos]; i++) {
    buffer[i] = n->neurons[pos][i].output;
  }
}

void activate(Network* n, float* inputs) {
  int max_outputs = find_max_outputs(n);
  float* calculated_outputs = malloc(sizeof(float)*max_outputs);
  
  //complete first forward propagation
  layer_activate(n, 0, inputs);
  
  //now propagate the rest of the layers
  for (int i = 1; i < n->network_depth; i++) {
    retrieve_outputs(n, i-1, calculated_outputs);
    layer_activate(n, i, calculated_outputs);
  }
  free(calculated_outputs);
}

float error_mse(Network* n, int expected) {
	int pos = n->network_depth-1;
	float output_number = (float) n->layer_depth[pos];
	float error = 0.0f;

	for (int i = 0; i < output_number; i++) {
    if (i == expected) {
		  error += powf(n->neurons[pos][i].output - 1, 2);
    } else {
		  error += powf(n->neurons[pos][i].output, 2);
    }    
	}
	
	return error/output_number;
}

int predict_from_input(Network* n, float* inputs) {
  activate(n, inputs);
  return 0;
}


void debug_csv_init_neuron(Neuron* n, FILE* o, int layer_pos, 
                            int neuron_number) {
  for (int i = 0; i < n->input_length; i++) {
    fprintf(o, "Layer %d Neuron %d Weight %d, ", layer_pos, neuron_number, i);
  }

    fprintf(o, "Layer %d Neuron %d Bias, ", layer_pos, neuron_number);
    fprintf(o, "Layer %d Neuron %d Near Output, ", layer_pos, neuron_number);
    fprintf(o, "Layer %d Neuron %d Output, ", layer_pos, neuron_number);
}

void debug_csv_init_layer(Network* n, int pos) {
  for (int i = 0; i < n->layer_depth[pos]; i++) {
    debug_csv_init_neuron(&n->neurons[pos][i], n->debug_csv, pos, i);
  }
}

void debug_csv_init(Network* n, int input_length) {
  fprintf(n->debug_csv, "Error, ");
  for (int i = 0; i < input_length; i++) {
    fprintf(n->debug_csv, "Input %d, ", i);
  }

  for (int i = 0; i < n->network_depth; i++) {
    debug_csv_init_layer(n, i);
  }

  fprintf(n->debug_csv, "\n");

}

void debug_csv_out_neuron(Neuron* n, FILE* o) {
	for (int i = 0; i < n->input_length; i++) {
		fprintf(o, "%f, ", n->weights[i]);
	}

	fprintf(o, "%f, ", n->bias);
	fprintf(o, "%f, ", n->near_output);
	fprintf(o, "%f, ", n->output);

}

void debug_csv_out_layer(Network* n, int pos) {
	int size = n->layer_depth[pos];
	for (int i = 0; i < size; i++) {
		debug_csv_out_neuron(&n->neurons[pos][i], n->debug_csv);
	}

}

void debug_csv_out(Network* n, float* inputs, int input_length, int expected) {
  //error
	fprintf(n->debug_csv, "%f, ", error_mse(n, expected));
	for (int i = 0; i < input_length; i++) {
		fprintf(n->debug_csv, "%f, ", inputs[i]);
	}

	for (int i = 0; i < n->network_depth; i++) {
		debug_csv_out_layer(n, i);
	}

	fprintf(n->debug_csv, "\n");

}

void init_dfa_batch_neuron(Neuron* n, int batch_quantity, int error_quantity) {
  n->batch_size = batch_quantity;
  n->learning_batch = malloc(sizeof(float*)*batch_quantity);
  
  for (int i = 0; i < n->batch_size; i++) {
    n->learning_batch[i] = malloc(sizeof(float) * (n->input_length + 1));
                                                              //+1 is the bias
  }
	//printf("Error Quantity: %d\n", error_quantity); 
  n->rows = n->input_length + 1; //+1 is the bias
	//printf("Rows: %d\n", n->rows); 
  n->columns = error_quantity; //doubles as error_quantity
  n->eb_matrix = malloc(sizeof(float*)*n->rows);
  n->error_output = malloc(sizeof(float) * n->rows); // this will make the error
                                                     // row at the end
  for (int i = 0; i < n->rows; i++) {
    n->eb_matrix[i] = malloc(sizeof(float) * n->columns);
  }

}

void generate_error_dfa_neuron(Neuron* n, float* errors) {

  //Generate the noise
  for (int rows = 0; rows < n->rows; rows++) {
    for (int columns = 0; columns < n->columns; columns++) {
      n->eb_matrix[rows][columns] = std_deviation_random();     
    }
  }
  
  for (int rows = 0; rows < n->rows; rows++) {
    n->error_output[rows] = 0;
    for (int columns = 0; columns < n->columns; columns++) {
      n->error_output[rows] += n->eb_matrix[rows][columns] * errors[columns];
    }
  }
}

void exec_dfa_neuron(Neuron* n, float* errors, float* inputs,int batch) {
  //set the error_output
  generate_error_dfa_neuron(n,errors);
  //@TODO
  //de output will need to be a switch case in the future for other types
  float de_output = n->output * (1 - n->output);
  
  for (int i = 0; i < n->rows-1; i++) { //-1 because the bias is done diff
    n->learning_batch[batch][i] = inputs[i] * de_output * n->error_output[i];
  }
  //bias
  n->learning_batch[batch][n->rows-1] = de_output * n->error_output[n->rows-1]; 
}

void learn_dfa_neuron(Neuron* n, float learning) {
  float size = (float) n->batch_size;

  for (int j = 0; j < n->batch_size; j++) {
    for (int i = 0; i < n->input_length; i++) {
      n->weights[i] -= (learning * n->learning_batch[j][i]) / size;  
    }
    n->bias -= (learning * n->learning_batch[j][n->input_length]) / size;
  }
}

void free_dfa_neuron(Neuron* n) {
  //can't be assed right now
}

void init_dfa_batch_network(Network* n, int batch_quantity) {
  int error_quantity = n->layer_depth[n->network_depth-1];

  for (int j = 0; j < n->network_depth; j++) {
    for (int i = 0; i < n->layer_depth[j]; i++) {
      init_dfa_batch_neuron(&n->neurons[j][i], batch_quantity, error_quantity);
    }
  }
}

void learn_dfa_network(Network* n, float learning) {
  
  for (int j = 0; j < n->network_depth; j++) {
    for (int i = 0; i < n->layer_depth[j]; i++) {
      learn_dfa_neuron(&n->neurons[j][i], learning);
    }
  }
}

void exec_dfa_network_layer(Network* n, int pos, float* inputs, float* errors,
                            int batch) {
  for (int i = 0; i < n->layer_depth[pos]; i++) {
    exec_dfa_neuron(&n->neurons[pos][i], errors, inputs, batch);
  }

}

//Happy days, I can go in any direction now!
void exec_dfa_network(Network* n, float* inputs, int batch) {
  //get the errors
  float* errors = malloc(sizeof(float) * n->layer_depth[n->network_depth-1]);
  retrieve_outputs(n, n->layer_depth[n->network_depth-1], errors); 
  
  //create the input buffer
  int max_outputs = find_max_outputs(n);
  float* input_buffer = malloc(sizeof(float) * max_outputs);

  //start by doing first layer
  exec_dfa_network_layer(n, 0, inputs, errors, batch);  
  //now do the rest of the layers
  for (int i = 1; i < n->network_depth; i++) {
    //get outputs of previous layer as the new inputs
    retrieve_outputs(n, i-1, input_buffer);
    exec_dfa_network_layer(n, i, input_buffer, errors, batch);
  }

  //free the buffers
  free(errors);
  free(input_buffer);
    
}



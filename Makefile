CC=gcc
CPPFLAGS= -g -Wall
NAME=network
LIB= -lm


network: obj/main.o obj/network.o obj/data.o
	$(CC) $(CPPFLAGS) -o bin/$(NAME) obj/main.o obj/network.o $(LIB)  

obj/main.o: src/main.c
	$(CC) $(CPPFLAGS) -c src/main.c -o obj/main.o

obj/network.o: src/network.c
	$(CC) $(CPPFLAGS) -c src/network.c -o obj/network.o

obj/data.o: src/data.c
	$(CC) $(CPPFLAGS) -c src/data.c -o obj/data.o

clean: 
	rm bin/$(NAME) 2> /dev/null && rm -rf obj 2> /dev/null && mkdir obj

run: network
	cd ./bin && ./$(NAME) && cd ..
